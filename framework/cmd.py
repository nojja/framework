import os
import sys
import subprocess

def execute(command):
	command_tab = [''+command]
	proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	(rescmd, err) = proc.communicate()
	
	return str(rescmd), str(err)
		
def compile(path, c_file, output_file):
        print "[*] Now compiling"
        execute("gcc "+path+c_file+" -o "+path+output_file)
