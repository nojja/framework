import os
import re
import fuzz
import readline

COMMANDS = ['use', 'search', 'help', 'exit',
            'set', 'show', 'exploit']

RE_SPACE = re.compile('.*\s+$', re.M)

class Completer(object):

    def _listdir(self, root):
        "List directory 'root' appending the path separator to subdirs."
        res = []
        for name in os.listdir(root):
            path = os.path.join(root, name)
            if os.path.isdir(path):
                name += os.sep
            res.append(name)
        return res

    def _complete_path(self, path=None):
        "Perform completion of filesystem path."
        if not path:
            return self._listdir('.')
        dirname, rest = os.path.split(path)
        tmp = dirname if dirname else '.'
        res = [os.path.join(dirname, p)
                for p in self._listdir(tmp) if p.startswith(rest)]
        # more than one match, or single match which does not exist (typo)
        if len(res) > 1 or not os.path.exists(path):
            return res
        # resolved to a single directory, so return list of files below it
        if os.path.isdir(path):
            return [os.path.join(path, p) for p in self._listdir(path)]
        # exact file match terminates this completion
        return [path + ' ']

    def complete_extra(self, args):
        "Completions for the 'extra' command."
        if not args:
            return self._complete_path('.')
        # treat the last arg as a path and complete it
        return self._complete_path(args[-1])

    def complete(self, text, state):
        "Generic readline completion entry point."
        buffer = readline.get_line_buffer()
        line = readline.get_line_buffer().split()
        # show all commands
        if not line:
            return [c + ' ' for c in COMMANDS][state]
        # account for last argument ending in a space
        if RE_SPACE.match(buffer):
            line.append('')
        # resolve command to the implementation function
        cmd = line[0].strip()
        if cmd in COMMANDS:
            impl = getattr(self, 'complete_%s' % cmd)
            args = line[1:]
            if args:
                return (impl(args) + [None])[state]
            return [cmd + ' '][state]
        results = [c + ' ' for c in COMMANDS if c.startswith(cmd)] + [None]
        return results[state]

def start(opt):
	while True:
		try:
			comp = Completer()
			readline.set_completer_delims(' \t\n;')
			readline.parse_and_bind("tab: complete")
			readline.set_completer(comp.complete)
			s = raw_input("(coffhex_shell)>>> ")
			s = s.strip().lower()
			s = s.split(" ")
		except:
			break

		if s[0] == "exit" :
			break
		if s[0] == "q" :
			break
		elif s[0] == "use" :
			if len(s) < 2 :
				print "[X] Missing exploit module name"
				continue
			if s[1] == "static_arg_bof":
				opt["exploit"]="static_arg_bof"
				opt["path"]=""
				opt["force"]=0
				opt["one_shot"]=0
				opt["requirements"]="unmet"
			else :
				print "[X] Bad exploit module name"
		elif s[0] == "set" :
			if len(s) < 2 :
				print "[X] Missing args"
				continue
			if not("exploit" in opt):
				print "[!] No exploit module selected"
				continue
			if s[1] == "path" :
				if len(s) < 3 :
					opt["path"] = ""
					continue
				opt["path"] = s[2]	
			elif s[1] == "force" or s[1] == "one_shot":
				if len(s) < 3 :
					print "[X] Missing integer value"
					continue
				if not(s[2].isdigit()) :
					print "[X] Bad integer value (0 or 1 only)"
					continue
				if not( 0 <= int(s[2]) <= 1 ) :
					print "[X] Bad integer value (0 or 1 only)"
					continue
				if s[1] == "force" :
					opt["force"] = int(s[2])
				if s[1] == "one_shot" :
					opt["one_shot"] = int(s[2])
			else :
				print "[X] option \"" + s[1] + "\" is not availabe"
				continue
			if opt["path"] != "" and 0 <= int(int(opt["force"])+int(opt["one_shot"])) <= 1 :
				opt["requirements"]="met"
			else :
				opt["requirements"]="unmet"
			
				
		elif s[0] == "show" :
			if s[1] == "options" :
				if "exploit" in opt :
					print "[*] Current options set"
					for key in opt :
						print key+" = "+str(opt[key])
		elif s[0] == "exploit" :
			if opt["requirements"] == "unmet":
				print "[X] Insufficient options to launch the exploit"
				continue
			else :
				if opt["force"]:
					fuzz.start(opt["path"], 1)
				else :
					fuzz.start(opt["path"], 2)

		elif s[0] == "search" :
			print "[*] Available exploit modules :"
			print "    >>> static_arg_bof : A static BSS buffer overflow exploit module based"
			print "                          on the first argument of an ELF"
			print " "
		elif s[0] == "help" :
			help()
		elif s[0] == "?" :
			help()			
		elif s == "":
			pass
		else :
			print "[!] Unknow command"

def help():
	print "Allowed commands in coffhex console:"
	print "    use <exploit_module> : set the curent exploit module"
	print "        set <OPTION> <VALUE> : set the OPTION of current exploit to VALUE"
	print "        show options : list the current available options for exploit module"
	print "        exploit : launch the exploit"
	print "    search : print available exploits"
	print "    help : print this help page"
	print "    exit : close current session "
