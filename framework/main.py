#! /usr/bin/python
import os
import sys
import fuzz
import console_mode

""" 
Ce fichier sert a parser les arguments au lancement du programme,
Il redirige l'utilisateur en fonction de ses choix
CLI = d'un coup sans prompt
Console = avec un petit prompt pour corriger les erreurs
"""

def main(arg):
	if len(arg) < 1 :
		err_in_args()

	elif "--help" in arg :
		help_page()


	elif "--cli" in arg :

		if "--console" in arg :
			err_in_args()
		elif len(arg) < 2 :
			err_in_args()		
		elif "--force" in arg :
			if len(arg) < 3 :
				err_in_args()
			if "--one-shot" in arg :
				err_in_args()
			for e in arg :
				if "/" in e :
					fuzz.start(e, 1)
		elif "--one-shot" in arg :
			for e in arg :
				if "/" in e :
					fuzz.start(e, 2)
		elif len(arg) == 2 :
			for e in arg :
				if "/" in e :
					fuzz.start(e, 0)
		

	elif "--console" in arg :
		os.system("clear")
		#Creation d'un objet pour la console
		opt = {}
		console_mode.start(opt)
		os.system("clear")		

	else :
		err_in_args()

def err_in_args():
	print "Try coffhex --help for more informations about usage"
	sys.exit(-1)

def help_page():
	print "Usage ./coffhex [--cli / --console] [--force / --one-shot] <Path to binary>"
	print "    --cli	: Use Coffhex in command line mode"
	print "    --console	: Use Coffhex in interactive console mode"
	print " "
	print "        --force		: Skip all the bin vuln checks"
	print "        --one-shot	: Skip all checks and disable prompts"
	print " "
	print " Dev by : Adrien Furet, Oussama Bavay, Raphael Kallini, Thomas Viaud"
	sys.exit(-2)

if __name__ == '__main__':
	main(sys.argv[1:])
