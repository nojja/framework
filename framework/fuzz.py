import os
import sys
import subprocess
import cmd
import exploit

def start(path, force_flag):
	check_path(path)
	flag = ""
	if force_flag :
		print "[!] Setting all the vuln flags to on due to --force arg"
		flag = "BOF"
	print "[*] Starting fuzzing module"
	print "[*] Analyzing >> "+path+" << binary"
	command = 'objdump -D '+path
	(rescmd, err) = cmd.execute(command)	
	for line in iter(rescmd.splitlines()) :
		if "scanf" in line or "strcpy" in line :
			flag = "BOF"
	if flag == "BOF" :
		flag_stack=""
		print "[~] Binary might be vulnerable to simple Buffer overflow"
		print "[*] Checking ASLR state"
		aslr = cmd.execute("cat /proc/sys/kernel/randomize_va_space")[0]
		print aslr
		print "[*] Checking if stack is executable"
		rescmd = cmd.execute("readelf -l "+ path)[0]
		for line in iter(rescmd.splitlines()) :
			if "GNU_STACK" in line :
				if " RW " in line :
					print "[X] Stack is not executable"
					flag_stack="RW"
				if " RWE " in line :
					print "[*] Stack is executable"
					flag_stack="RWE"
		if flag_stack == "RWE":
			if int(aslr) == 0 :
				print "[*] Calling simple buffer overflow module"
				exploit.static_arg_buffer_overflow(path, force_flag)
			else :
				print "[*] Calling ASLR buffer overflow module"
				exploit.aslr_arg_buffer_overflow(path, force_flag)

		elif flag_stack == "RW":
			print "[*] Calling ret2libc module"
			exploit.ret2libc_buffer_overflow(path, force_flag)
		else :
			print "[X] Could not determine stack state"
			print "[~] Calling simple buffer overflow module as last resort"
			exploit.static_arg_buffer_overflow(path)
	else :
		print "[!] Tests are showing that binary might not be vulnerable"
		print "[?] If you still want to proceed, add --force to your command"
		print "[X] Exiting"
		sys.exit(-4)

def check_path(path):
	if os.path.exists(path) :
		if path != "." :
			if path != ".." :
				if path != "/" :
					pass
				else :
					err_path()
			else :
				err_path()
		else :
			err_path()
	else :
		err_path()

def err_path():
        print "Error in path, or file doesn't exist. chek --help for more informations"
        sys.exit(-3)
